package Part1;

import CanvasFiles.MyList;
import java.util.Iterator;
import java.util.ListIterator;

// MERK:
// Alle metoder som tar inn en index skal kaste indexoutofbound exception
//
public class TwoWayLinkedList<E> implements MyList<E> {
	Node<E> head, tail;
	int size = 0;

	public TwoWayLinkedList() {
	}

	public TwoWayLinkedList(E[] objects) {
		for (int i = 0; i < objects.length; i++)
			addLast(objects[i]);
	} //peter

	public E getFirst() {
		if (size == 0) return null;
		return head.element;
	} //peter

	public E getLast() {
		if (size == 0) return null;
		return tail.element;
	} //peter

	public void addFirst(E e) {
		if (head == null) head = new Node<E>(e);
		else {
			Node<E> newHead = new Node<>(e);
			Node<E> oldHead = head;
			newHead.next = oldHead;
			oldHead.previous = newHead;
			size++;
		}
	} //peter

	public void addLast(E e) {
		if (tail == null) head = tail = new Node<E>(e);
		else {
			tail.next = new Node<>(e);
			(tail.next).previous = tail;
			tail = tail.next;
		}
		size++;
	} //peter

	public void add(int index, E e) throws IndexOutOfBoundsException {
		if (index >= size || size == 0) addLast(e);
		else if (index <= 0) addFirst(e);
		else {
            LinkedListIterator iterator = (LinkedListIterator) this.listIterator(index);
            iterator.current.element = e;
        }
	} //peter TODO: iterate with iterator

	public E removeFirst() {
		if (size == 0) return null;
		else {
			Node<E> tmpNode = head;
			head = head.next;
			head.previous = null;
			size--;
			return tmpNode.element;
		}
	}//peter

	public E removeLast() {
		if (size == 0) return null;
		else {
			Node<E> tempNode = tail;
			tail = tail.previous;
			tail.next = null;
			size--;
			return tempNode.element;
		}
	} //peter

	public E remove(int index) {
		if (index >= size || index < 0) return null;
		else if (index == 0) return removeFirst();
		else if (index == size - 1) return removeLast();
		else {
			Node<E> current;
			if (size / 2 <= index) { //start i korteste ende.
				current = tail;
				for (int i = size - 2; i >= index; i--)
					current = current.previous; //TODO: dobbelsjekk at vi tar rett index
			} else {
				current = head;
				for (int i = 1; i <= index; i++)
					current = current.next;
			}
			current.previous.next = current.next;
			current.next.previous = current.previous;
			size--;
			return current.element;
			//TODO: iterator stuff
		}
	} //peter TODO: iterate with iterator

	@Override
	public String toString() {
		Node<E> current = head;
		StringBuilder result = new StringBuilder("[");

		while (current != null) {
			result.append(current.element);
			current = current.next;
			if (current != null) {
				result.append(", ");
			}
		}
		result.append("]");
		return result.toString();
	} //peter

	public void clear() {
		size = 0;
		head = tail = null;
	} //peter

	public boolean contains(Object e) {
		if (size == 0) return false;
		LinkedListIterator iterator = (LinkedListIterator) this.listIterator();
		while (iterator.hasNext()){
		    if(e == iterator.next()) return true;
        }
		return false;
	} //peter

	public int indexOf(Object e) {
		if (size == 0) return -1;
        LinkedListIterator iterator = (LinkedListIterator) this.listIterator();
        while (iterator.hasNext()){
            if (iterator.next()==e)return iterator.index;
        }
		return -1;

	} //peter TODO: iterate with iterator

	public int lastIndexOf(Object e) {
		if (size == 0) return -1;
        LinkedListIterator iterator = (LinkedListIterator) this.listIterator(size-1);
        while (iterator.hasPrevious()){
            if (iterator.previous()==e)return iterator.index;
        }
        return -1;
	} //peter TODO: iterate with iterator

	public E get(int index) {
		if (size == 0) return null;
        LinkedListIterator iterator = (LinkedListIterator) this.listIterator(index);
        return (E)iterator.current.element;
    } //peter TODO: iterate with iterator

	public E set(int index, E e) {
		if (size == 0) return null;
        LinkedListIterator iterator = (LinkedListIterator) this.listIterator(index);
        iterator.set(e);
        return (E)iterator.current.element;
    } //peter TODO: iterate with iterator

	@Override
	public int size() {
		return size;
	} //peter

	public ListIterator<E> listIterator() {
		return new LinkedListIterator();
	} // TODO: er dette rett?

	public ListIterator<E> listIterator(int index) {
		return new LinkedListIterator(index);
	}// TODO: er dette rett?

	@Override
	public Iterator<E> iterator() {
		return new LinkedListIterator() ;
	}// TODO: er dette rett?


	private class LinkedListIterator<E> implements java.util.ListIterator<E> {
		// datamedlemmer
		int index=0;
		private Node<E> current;


		public LinkedListIterator(){
         current = (Node<E>)head;
		} //peter
		public LinkedListIterator(int index) {
			this.index=index;
			current = (Node<E>)head;
            for (int i = 0; i < index; i++) {
                current = current.next;
            }
		} //peter

		public void setLast() {
        }// TODO: What does it mean? ka vi ska gjør her?

		@Override
		public boolean hasNext() {
			return current.next != null;
		} //peter

		@Override
		public E next() {
		    index++;
		    current = current.next;
			return current.element;
		} //peter

		@Override
		public boolean hasPrevious() {
		    return current.previous != null;
		} //peter

		@Override
		public E previous() {
		    index--;
		    current = current.previous;
			return current.element;
		} //peter

		@Override
		public int nextIndex() {
		    if (!hasNext())return index;
			return index+1;
		} //peter

		@Override
		public int previousIndex() {
            if (!hasPrevious())return index;
			return index-1;
		} //peter

		@Override
		public void remove() {
		    current.previous.next= current.next;
		    current.next.previous = current.previous;

		} //peter
		@Override
		public void set(E e) {
		    current.element = e;
		} //peter

		@Override
		public void add(E e) {
            Node<E> insertedNode = new Node<E>(e);

            insertedNode.previous=current.previous;
            insertedNode.next=current;
            current.previous.next=insertedNode;
            current.previous=insertedNode;
		} //peter
	}


	private class Node<E> {
		E element;
		Node<E> next;
		Node<E> previous;

		public Node(E o) {
			element = o;
		}
	} //peter


}



