package Part1;

import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

public class TwoWayLinkedListTest {
	TwoWayLinkedList list;
	@Before
	public void before() {
		list = new TwoWayLinkedList<>();
		list.addLast("hei");
		list.addLast("du");
		list.addLast("er");
		list.addLast(22);
		list.addLast("år");
		list.addLast(22);
		list.addLast("år");
	}

	@Test
	public void sizeIs5() {
		assertEquals(7,list.size);
	}

	@Test
	public void removeFirst() {
		assertEquals("hei",list.removeFirst());
	}

	@Test
	public void removeLast() {
		assertEquals("år",list.removeLast());
	}

	@Test
	public void contains22() {
		assertEquals(true,list.contains(22));
	}

	@Test
	public void indexOf22() {
		assertEquals(3,list.indexOf(22));
	}

	@Test
	public void lastIndexOF22() {
		assertEquals(5,list.lastIndexOf(22));
	}

	@Test
	public void testGetIndex3() {
		assertEquals(22,list.get(3));
	}
}